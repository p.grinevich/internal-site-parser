#!/bin/bash

latest_folder=$(ls -td ./db/*/ | head -n 1)

source_folder="${latest_folder}res_storage/"
target_folder="./db/old/res_storage/"

mkdir -p $target_folder

mv $source_folder*.txt $target_folder
