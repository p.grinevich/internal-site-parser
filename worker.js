const {parentPort, workerData} = require('worker_threads');
const fetch = require('node-fetch');
const {JSDOM} = require('jsdom');
const fs = require('fs').promises;
const async = require('async');

const MAX_CONCURRENT_TASKS = 15;
const visitedLinks = new Set();
const urlsAwaitVisit = new Set();
const urls = workerData['urlsToParse'];
const mode = workerData['mode'];
const allowTld = workerData['allowTld'];
const dbName = workerData['dbName'];
const OLD_DB = './db/old';

async function run() {
    await runParallelParsing(urls);
    //parentPort.postMessage('Done parsing'); // send message to main thread when done
}

async function parseWebsite(url) {
    const used = process.memoryUsage();
    //console.log(`Memory usage: ${Math.round(used.heapUsed / 1024 / 1024 * 100) / 100} MB`);
    //console.log('urlsAwaitVisit :' + urlsAwaitVisit.size);
    //console.log('visitedLinks : ' + visitedLinks.size);

    if (visitedLinks.has(url)) {
        const nextLink = urlsAwaitVisit.values().next().value;
        urlsAwaitVisit.delete(nextLink);
        await enqueueUrls(nextLink);

        return;
    }

    visitedLinks.add(url);

    try {
        const response = await fetch(url, {
            headers: {
                'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/W.X.Y.Z Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
            },
        });

        if (!response.ok) {

            //console.error(`Error fetching ${url}: ${response.status} ${response.statusText}`);
            const nextLink = getNextLink();

            if (nextLink) {
                await enqueueUrls(nextLink);
            }

            return;
        }

        const html = await response.text();
        const {window} = new JSDOM(html);
        let {document} = window;
        const domain = new URL(url).hostname;

        const key = document.querySelector('p').textContent.trim(); // headerLayout.querySelector('p').textContent.trim();

        if (!key || key && key.length <= 3) {
            const nextLink = getNextLink();
            if (nextLink) {
                await enqueueUrls(nextLink);
            }
            return;
        }

        const nameFile = encodeURIComponent(key);
        const h3 = document.querySelector('h3'); //document.querySelector('h3');

        const resStoragePath = `${OLD_DB}/res_storage/${nameFile}.txt`;
        const keysPath = `${OLD_DB}/key_storage/keys.txt`;

        try {
            let resFile = await fs.readFile(resStoragePath);
            parentPort.postMessage('exist');

            const nextLink = getNextLink();
            await enqueueUrls(nextLink);
            return;
        } catch (err) {
            const newResStoragePath = `./db/${dbName}/res_storage/${nameFile}.txt`;
            const newKeysPath = `./db/${dbName}/key_storage/keys.txt`;

            try {
                let resFile = await fs.readFile(newResStoragePath);
                parentPort.postMessage('exist');
                const nextLink = getNextLink();
                await enqueueUrls(nextLink);
                return;
            } catch (err) {
                await fs.appendFile(newKeysPath, key + '\n');
                await fs.appendFile(newResStoragePath, h3.textContent + '\n');
            }
        }

        const links = Array.from(h3.getElementsByTagName('a'));

        const validLinks = links
            .map((link) => link.getAttribute('href'))
            .filter((link) => isValidUrl(link) && !visitedLinks.has(link));

        const lastLink = validLinks.pop();
        await enqueueUrls(lastLink);
        validLinks.forEach((link) => urlsAwaitVisit.add(link));

        response.body.destroy();
        window.close();
        document = null;
    } catch (err) {
        console.error(`Error parsing ${url}: ${err.message}`);
        const nextLink = getNextLink();
        if (nextLink) {
            await enqueueUrls(nextLink);
        }
    }
}

function getNextLink() {
    const nextLink = urlsAwaitVisit.values().next().value;
    urlsAwaitVisit.delete(nextLink);
    return nextLink;
}

function isValidUrl(url) {
    try {
        url = new URL(url);
        const domain = url.hostname;
        const tld = domain.slice(domain.lastIndexOf('.') + 1);
        const isXml = /\.xml$/i.test(url);

        if (mode === 'internal-domains') {
            return urls.some(currentUrl => new URL(currentUrl).hostname === domain) && !isXml;
        }

        if (mode === 'only-specific-tld') {
            return allowTld.includes(tld);
        }
    } catch (err) {
        return false;
    }
}

async function enqueueUrls(url) {
    const queue = async.queue(async (url, callback) => {
        await parseWebsite(url);
        callback();
    }, MAX_CONCURRENT_TASKS);

    queue.drain(() => {
        //parentPort.postMessage('urlsAwaitVisit : ' + urlsAwaitVisit.size);
        //parentPort.postMessage('visitedLinks : ' + visitedLinks.size);
        // console.log('All tasks completed');
    });

    queue.push(url);
}

async function runParallelParsing(urls) {
    await enqueueUrls(urls);
    while (urlsAwaitVisit.size > 0) {
        const urlsToVisit = Array.from(urlsAwaitVisit).splice(0, MAX_CONCURRENT_TASKS);
        await Promise.all(urlsToVisit.map((url) => parseWebsite(url)));
    }
}

run();
