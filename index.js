const {Worker} = require('worker_threads');
const crypto = require('crypto');
const fs = require("fs");
const {join} = require("path");

const DB_PATH = './db';
const mode = process.argv.slice(2)[0];
const urls = loadArrFromFile('./domains.txt');
const allowTld = loadArrFromFile('./allow_tld.txt');

let dbName = process.argv.slice(3)[0];
let allExistsKey = 0;

checkMode();

if (dbName) {
    createDb();
} else {
    dbName = getLastModifiedDirectory(DB_PATH);
}

urls.forEach((url) => {
    const urlsToParse = Array.from({length: 15}, () => `${url}/${randomString()}`);
    const worker = new Worker('./worker.js', {
        workerData: {
            'urlsToParse' : urlsToParse,
            'mode' : mode,
            'allowTld' : allowTld,
            'dbName' : dbName
        }
    });

    worker.on('message', (message) => {
        if (message === 'exist') {
            allExistsKey++;
        }
        console.log(`${message}`);
        console.log(`existsKey : ${allExistsKey}`);
    });

    worker.on('error', (error) => {
        console.error(`Worker error: ${error.stack}`);
    });

    worker.on('exit', (code) => {
        if (code !== 0) {
            console.error(`Worker stopped with exit code ${code}`);
        }
    });
});

function randomString(size = 5) {
    return crypto.randomBytes(size).toString('hex').slice(0, size);
}

function checkMode() {
    if (['internal-domains', 'only-specific-tld'].includes(mode)) {
        console.log(`Mode is set to: ${mode}`);
    } else {
        console.log('Invalid mode');
        process.exit(1);
    }
}

function loadArrFromFile(filePath) {
    try {
        const fileContent = fs.readFileSync(filePath, 'utf-8');
        return fileContent.split('\n').map((line) => line.trim()).filter((line) => line !== '');
    } catch (error) {
        console.error(`Error loading from file: ${error}`);
        return [];
    }
}

function createDb() {
    const dbFolderPath = join(DB_PATH, dbName);
    const keyStoragePath = join(dbFolderPath, 'key_storage');
    const resStoragePath = join(dbFolderPath, 'res_storage');
    const keyFilePath = join(keyStoragePath, 'keys.txt');

    try {
        fs.mkdirSync(dbFolderPath);
        fs.mkdirSync(keyStoragePath);
        fs.mkdirSync(resStoragePath);
        fs.writeFileSync(keyFilePath, '');

        return dbName;
    } catch (error) {
        console.error(`Error while creating database folder: ${error}`);
        return null;
    }
}

function getLastModifiedDirectory(folderPath) {
    try {
        const files = fs.readdirSync(folderPath);
        let latestDir = null;
        let latestTime = 0;

        for (const file of files) {
            const filePath = join(folderPath, file);
            const stats = fs.statSync(filePath);

            if (stats.isDirectory() && stats.ctimeMs > latestTime) {
                latestDir = filePath;
                latestTime = stats.ctimeMs;
            }
        }

        return latestDir;
    } catch (error) {
        console.error(`Error while finding latest db directory: ${error}`);
        return false;
    }
}


