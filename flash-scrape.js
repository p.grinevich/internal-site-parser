const { Worker } = require('worker_threads');
const crypto = require('crypto');
const worker = require('/var/www/parser-website/worker.js');
function randomString(size = 5) {
    return crypto.randomBytes(size).toString('hex').slice(0, size);
}

const urls = [
    'https://darmspiegelung-frechen.de',
    'https://ilbirrificiodivarese.eu',
    'https://wohnen-weserauen.de',
    'https://navimow-segway.de',
    'https://vanmeijel-strassenmobiliar.de',
    'https://healy4u.de',
];

urls.forEach((url) => {
    const urlsToParse = Array.from({length: 15}, () => `${url}/${randomString()}`);
    const worker = new Worker(worker, {
        workerData: urlsToParse
    });

    worker.on('message', (message) => {
        console.log(`Message from worker: ${message}`);
    });

    worker.on('error', (error) => {
        console.error(`Worker error: ${error}`);
    });

    worker.on('exit', (code) => {
        if (code !== 0) {
            console.error(`Worker stopped with exit code ${code}`);
        }
    });
});
